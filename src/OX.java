import java.util.Scanner;
import java.util.InputMismatchException;

public class OX {
	static String board[][], turn, win;
	static Scanner kb = new Scanner(System.in);
	
	public static void main(String[] args) {
		printStart();
		printBoard();
		inputPosition();
		printTurn();
	}
	
	static void printTurn() {
		while (win == null) {
			int numInR = 0;
			int numInC = 0;
			try {
				numInR = kb.nextInt();
				numInC = kb.nextInt();
				if(!(numInC>0 && numInC <=3) || !(numInR>0 && numInR<=3)) {
					System.out.println("Row and Column must be number 1 - 3");
					continue;
				}
			}catch (InputMismatchException e) {
				System.out.println("Row and Column must be number");
			}
			if(board[numInR-1][numInC-1] != "O" && board[numInR-1][numInC-1] != "X") {
				board[numInR-1][numInC-1] = turn;
				if (turn.equals("X")) {
					turn = "O";
				}else {
					turn = "X";
				}
				printBoard();
				win = checkWin();
			}else {
				System.out.print("Row "+numInR+" and Column "+(numInC-1)+" can't choose again");
				continue;
			}
		}
		if (win.equalsIgnoreCase("draw")) {
			
		} else {
			System.out.println(win + " : WIN");
		}
	}
	
	static void printStart(){
		System.out.println("Start Game OX");
		board = new String[3][3];
		win = null;
		turn = "X";
		
		for(int i=0; i<board.length; i++) {
			for(int j=0; j<board.length; j++ ) {
				board[i][j] = " "; 
			}
		}
	}
	
	static void inputPosition(){
		System.out.println("Turn: " + turn + "\n" + "Plz choose position (R,C): ");
	}

	static String checkWin() {
		for(int i=0; i<8; i++) {
			String line = null;
			switch (i) {
			case 0:
				line = board[0][0] + board[0][1] + board[0][2];
				break;
			case 1:
				line = board[1][0] + board[1][1] + board[1][2];
				break;
			case 2:
				line = board[2][0] + board[2][1] + board[2][2];
				break;
			case 3:
				line = board[0][0] + board[1][0] + board[2][0];
				break;
			case 4:
				line = board[0][1] + board[1][1] + board[2][1];
				break;
			case 5:
				line = board[0][2] + board[1][2] + board[2][2];
				break;	
			case 6:
				line = board[0][0] + board[1][1] + board[2][2];
				break;
			case 7:
				line = board[2][0] + board[1][1] + board[0][2];
				break;
			}
			if (line.equals("XXX")) {
				return "X";
			}else if (line.equals("OOO")) {
				return "O";
			}
		}
		inputPosition();
		return null;
	}

	static void printBoard() {
		System.out.println("  1 2 3");
		for(int i=0; i<board.length; i++) {
			System.out.print(i+1);
			for(int j=0; j<board.length; j++ ) {
				System.out.print("|" + board[i][j]);
			}
			System.out.print("|");
			System.out.println();
		}
	}

}
